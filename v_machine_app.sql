-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2019 at 02:33 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `v_machine_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `beverages`
--

CREATE TABLE `beverages` (
  `beverages_id` int(11) NOT NULL,
  `beverages_name` varchar(30) NOT NULL,
  `beverages_cost` int(4) NOT NULL,
  `beverages_status` varchar(50) NOT NULL DEFAULT 'availlable'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `beverages`
--

INSERT INTO `beverages` (`beverages_id`, `beverages_name`, `beverages_cost`, `beverages_status`) VALUES
(2, 'black_tea_without_sugar', 60, 'availlable'),
(3, 'coffe_simple', 30, 'availlable'),
(4, 'coffee_simple_without_sugar', 40, 'availlable'),
(6, 'black_coffee', 60, 'availlable');

-- --------------------------------------------------------

--
-- Table structure for table `beveragesingredients`
--

CREATE TABLE `beveragesingredients` (
  `ingredients_id` int(11) NOT NULL,
  `beverages_id` int(11) NOT NULL,
  `ingredients_unit` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `beveragesingredients`
--

INSERT INTO `beveragesingredients` (`ingredients_id`, `beverages_id`, `ingredients_unit`) VALUES
(1, 3, 5),
(1, 4, 5),
(1, 6, 5),
(2, 2, 2),
(2, 3, 2),
(2, 4, 2),
(2, 6, 2),
(3, 2, 2),
(3, 3, 2),
(3, 4, 2),
(3, 6, 2),
(4, 2, 2),
(4, 3, 2),
(4, 4, 2),
(4, 6, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ingredients`
--

CREATE TABLE `ingredients` (
  `ingredients_id` int(11) NOT NULL,
  `ingredients_name` varchar(20) NOT NULL,
  `ingredients_instock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ingredients`
--

INSERT INTO `ingredients` (`ingredients_id`, `ingredients_name`, `ingredients_instock`) VALUES
(1, 'milk', 50),
(2, 'water', 50),
(3, 'sugar', 40),
(4, 'coffee', 80);

-- --------------------------------------------------------

--
-- Table structure for table `orderdetails`
--

CREATE TABLE `orderdetails` (
  `orderid` int(11) NOT NULL,
  `ingredients_id` int(11) DEFAULT NULL,
  `order_amount` int(11) NOT NULL,
  `beverages_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orderdetails`
--

INSERT INTO `orderdetails` (`orderid`, `ingredients_id`, `order_amount`, `beverages_id`) VALUES
(1, NULL, 60, 2),
(2, NULL, 60, 2),
(3, NULL, 60, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `beverages`
--
ALTER TABLE `beverages`
  ADD PRIMARY KEY (`beverages_id`);

--
-- Indexes for table `beveragesingredients`
--
ALTER TABLE `beveragesingredients`
  ADD PRIMARY KEY (`ingredients_id`,`beverages_id`),
  ADD KEY `beverages_id` (`beverages_id`);

--
-- Indexes for table `ingredients`
--
ALTER TABLE `ingredients`
  ADD PRIMARY KEY (`ingredients_id`);

--
-- Indexes for table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD PRIMARY KEY (`orderid`),
  ADD KEY `beverages_id` (`beverages_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `beverages`
--
ALTER TABLE `beverages`
  MODIFY `beverages_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ingredients`
--
ALTER TABLE `ingredients`
  MODIFY `ingredients_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orderdetails`
--
ALTER TABLE `orderdetails`
  MODIFY `orderid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `beveragesingredients`
--
ALTER TABLE `beveragesingredients`
  ADD CONSTRAINT `beveragesingredients_ibfk_1` FOREIGN KEY (`ingredients_id`) REFERENCES `ingredients` (`ingredients_id`),
  ADD CONSTRAINT `beveragesingredients_ibfk_2` FOREIGN KEY (`beverages_id`) REFERENCES `beverages` (`beverages_id`);

--
-- Constraints for table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD CONSTRAINT `orderdetails_ibfk_1` FOREIGN KEY (`ingredients_id`) REFERENCES `ingredients` (`ingredients_id`),
  ADD CONSTRAINT `orderdetails_ibfk_2` FOREIGN KEY (`beverages_id`) REFERENCES `beverages` (`beverages_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
