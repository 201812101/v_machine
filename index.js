const express= require("express");
const bodyparser  = require("body-parser");
const rest= require("./routes");
const config= require("./config");
const app = express();
app.use(bodyparser.urlencoded({extended:true}));
app.use(bodyparser.json());
app.use('/vmachine',rest);
app.listen(process.env.port|| config.port,()=>{
        console.log("All right I am alive at port no 3000");
});