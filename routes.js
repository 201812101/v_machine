const express = require('express');
const router = express.Router();
const mysql = require("mysql");
const config = require('./config');
var pool = mysql.createPool(config.mysql);

router.get("/", function (req, res) {
        res.json({
                "message": "hello there!"
        });
});

//items                
router.get("/getallitems", function (req, res) {
        var array = [];
        var query = "SELECT * FROM beverages";
        query = mysql.format(query);
        pool.query(query, (err, rows) => {
                if (err) {
                        return res.json({
                                "Error": "true",
                                "message": "error getting data from beverages"
                        });
                }
                if (rows.length != 0) {
                        for (i = 0; i < rows.length; i++) {
                                var query = "select ingredients_id,ingredients_unit,beverages_id from beveragesingredients where beverages_id=?";
                                var table = [rows[i]['beverages_id']];
                                query = mysql.format(query, table);
                                pool.query(query, (err, results) => {
                                        if (err) {
                                                return;
                                        }
                                        else {
                                                for (i = 0; i < results.length; i++) {
                                                        var query = "select ingredients_instock from ingredients where ingredients_id=?";
                                                        var table = [results[i]['ingredients_id']];
                                                        query = mysql.format(query, table);
                                                        pool.query(query, (err, resul) => {
                                                                if (err) {
                                                                        console.log(err);
                                                                } else {

                                                                        if (resul[0]['ingredients_instock'] < results[0]['ingredients_instock']) {
                                                                                
                                                                        } else {
                                                                                
                                                                        }

                                                                }
                                                        });

                                                }
                                        }
                                });
                                //   console.log(q.results);
                        }
                }
                res.json({
                        "Error": "false",
                        "message": "sucess",
                        "items": rows
                });
        });
});

router.post("/additem", function (req, res) {
        var query = "INSERT INTO beverages(??,??) VALUES(?,?)";
        var table = ["beverages_name", "beverages_cost", req.body.beverages_name, req.body.beverages_price];
        query = mysql.format(query, table);
        pool.query(query, (err, rows) => {
                if (err) {
                        console.log(err);
                        return res.json({
                                "error": "true",
                                "message": "can not  insert beverages data"

                        });
                }
                res.json({ "error": "false", "message": "success!-beverages added" });
        });
});

router.put("/beverages/:id/", function (req, res) {
        var query = "UPDATE beverages SET ??=? WHERE ??=?";
        var table = ["beverages_cost", req.body.beverages_price, "beverages_id", req.params.id];
        query = mysql.format(query, table);
        pool.query(query, (err, results) => {
                if (err) {
                        return res.json({
                                "error": "true",
                                "message": "can not update"
                        });
                }
                res.json({
                        "error": "false",
                        "message": "success",
                        "result": results
                });
        });

});

router.delete("/beverages/:b_name", function (req, res) {
        var query = "DELETE FROM ?? WHERE ??=?";
        var table = ["beverages", "beverages_name", req.params.b_name];
        query = mysql.format(query, table);
        console.log(query);
        pool.query(query, (err, result) => {
                if (err) {
                        console.log(err);
                        return res.json({ "error": "true", "message": "can not delete please check id" });
                }
                res.json({
                        "error": "false",
                        "message": "successfully Deleted!"
                });
        });
});



//ingredients op
router.get("/getingredients", function (req, res) {
        var query = "SELECT * FROM ingredients";
        query = mysql.format(query);
        pool.query(query, (err, rows) => {
                if (err) {
                        return res.json({
                                "Error": "true",
                                "message": "error getting data from beverages"
                        });
                }
                res.json({
                        "error": "false",
                        "message": "success",
                        "results": rows
                });
        });
});
router.post("/addingredients", function (req, res) {
        var query = "INSERT INTO ??(??,??) VALUES(?,?)";
        var table = ["ingredients", "ingredients_name", "ingredients_instock", req.body.ingredients_name, req.body.ingredients_instock];
        query = mysql.format(query);
        pool.query(query, (err, rows) => {
                if (err) {
                        return res.json({
                                "error": "true",
                                "message": "Can not insert"
                        });
                }
                res.json({
                        "error": "false",
                        "message": "success",
                        "result": rows
                });
        });
});
router.put("/ingredients/:id", function (req, res) {

        var query = "UPDATE ingredients SET ??=? WHERE ??=?";
        var table = ["ingredients_instock", req.body.ingredients_instock, "ingredients_id", req.params.id];
        query = mysql.format(query, table);
        pool.query(query, (err, rows) => {
                if (err) {
                        return res.json({
                                "error": "true",
                                "message": "Can not update"
                        });
                }
                res.json({
                        "error": "false",
                        "message": "success",
                        "result": rows
                });

        });
});

//order op
router.post("/order", function (req, res) {

        var beveragesID = req.body.beverages_id;
        var query = "SELECT * FROM beverages WHERE beverages_id=?";
        var table = [beveragesID];
        query = mysql.format(query, table);
        pool.query(query, (err, row) => {
                if (err) {
                        return res.json({
                                "error": "true",
                                "message": "can not get cost of the beverages"
                        });
                }
                query = "INSERT INTO orderdetails(??,??) VALUES(?,?)";
                table = ["beverages_id", "order_amount", beveragesID, row[0]['beverages_cost']];
                // console.log(row[0]['beverages_cost']);
                query = mysql.format(query, table);
                pool.query(query, (err, row) => {
                        if (err) {
                                return res.json({
                                        "error": "true",
                                        "message": "can not order"
                                });
                        }
                        else {

                                //call the vending machine function here

                        }
                        res.json({
                                "error": "false",
                                "message": "success"
                        });
                });


        });

});


//create  recipe
router.post("/createrecipe", function (req, res) {
        console.log(req.body.beverages_id);
        console.log(req.body.ingredients);
        var query = "select * from beveragesingredients where beverages_id=?";
        var table = [req.body.beverages_id];
        query = mysql.format(query, table);
        pool.query(query, function (err, results) {
                if (err) {
                        return res.json({ "Error": "true" });

                } else {
                        if (results.length == 0) {
                                for (i = 0; i < req.body.ingredients.length; i++) {
                                        var query = "INSERT INTO ??(??,??,??) VALUES(?,?,?)";
                                        var table = ["beveragesingredients", "ingredients_id", "beverages_id", "ingredients_unit",
                                                req.body.ingredients[i]['ingredients_id'], req.body.beverages_id, req.body.ingredients[i]['ingredients_unit']];
                                        query = mysql.format(query, table);
                                        pool.query(query, function (err, row) {

                                                if (err) {

                                                        if (err.sqlMessage.includes("Duplicate")) {

                                                                return res.json({
                                                                        "error": "true",
                                                                        "message": "succes"
                                                                });
                                                        }
                                                }


                                        });
                                }


                        } else {
                                return res.json({ "message": "use this api for creation only for update use updaterecepie" });
                        }

                }
        });


        //         return res.json({
        //                 "error":"true",
        //                 "message":"duplicate entry"
        //         });



});



module.exports = router;